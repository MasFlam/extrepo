#!/usr/bin/perl -w

use strict;
use warnings;

use ExtUtils::MakeMaker;

WriteMakefile(
	NAME => "Debian::ExtRepo",
	VERSION_FROM => "lib/Debian/ExtRepo.pm",
	PREREQ_PM => {
		'LWP::UserAgent' => 0,
		'YAML' => 0,
		'File::Temp' => 0,
	},
	EXE_FILES => [ 'extrepo' ],
);
